import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  todoList = [];

  addToDo(value) {
    if(value !== '') {
      if(this.todoList.indexOf(value) === -1) {
        this.todoList.push(value);
      }
      else {
        alert('Item with this name already exists');
      }
    }
    else {
      alert('Input field can\'t be an empty');
    }
  }

  deleteItem(todo) {
    console.log('here');
    this.todoList.splice(this.todoList.indexOf(todo), 1);
  }
}